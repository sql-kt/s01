Session 1 Activity
1. List the books authored by Marjorie Green 213-46-8915

BU1032 The Busy Executive's Database Guide
BU2075 You Can Combat Computer Stress!

2. List the books authored by Michael O'Leary 267-41-2394

BU1111 Cooking with Computers
TC7777 (Not Listed in the Title Table)

3. Write the author/s of "The Busy Executive's Database Guide" BU1032

213-46-8915 Green, Marjorie
409-56-7008 Bennet, Abraham

4. Identify the publisher of "But Is It User Friendly?" PC1035

1389 Algodata Infosystems

5. List the books published by Algodata Infosystems 1389

BU1032 The Busy Executive's Database Guide
BU1111 Cooking with Computers
BU7832 Straight Talk About Computers
PC1035 But Is It User Friendly?
PC8888 Secrets of Silicon Valley
PC9999 Net Etiquette